﻿using MathAPI.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;

namespace MathAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MathController : ControllerBase
    {
        private readonly IAddService _addService;
        private readonly ICircleService _circleService;
        private readonly IBinominalService _binominalService;
        private readonly IFibonacciService _fibonacciService;
        
        public MathController(IAddService addService, IBinominalService binominalService, IFibonacciService fibonacciService, ICircleService circleService)
        {
            _addService = addService;
            _binominalService = binominalService;
            _fibonacciService = fibonacciService;
            _circleService = circleService;
        }

        [HttpGet("add/{a}/{b}")]
        public IActionResult Get(double a, double b)
        {
            return Ok(new
            {
                elementOne = a,
                elemetTwo = b,
                sum = _addService.Add(a, b)
            });
        }

        [HttpGet("circleField/{a}")]
        public IActionResult GetCircle(double r)
        {
            return Ok(new
            {
                elementOne = r,
                circleField = _circleService.CircleField(r),
                circleCircuit = _circleService.CircleCircuit(r)
            });
        }

        
        [HttpGet("binominal-coefficient/{n}/{k}")]
        public IActionResult GetBinominalCoefficient(int n, int k)
        {
            return Ok(new
            {
                n,
                k,
                result = _binominalService.GetBinominal(n, k)
            });
        }

        [HttpGet("fibonacci-calculate/{a}")]
        public IActionResult Get(int a)
        {
            if (a < 0)
                return BadRequest("Liczba musi byc wieksza od zera");

            try
            {
                var resultFromService = _fibonacciService.GetFibonacci(a);

                return Ok(resultFromService);
            }
            catch (Exception err)
            {
                return BadRequest($"Błąd: {err.Message}");
            }

        }
    }
}
