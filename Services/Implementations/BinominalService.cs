﻿using MathAPI.Services.Interfaces;
using System;

namespace MathAPI.Services.Implementation
{
    public class BinominalService : IBinominalService
    {
        public long GetBinominal(long n, long k)
        {
            // Szef mnie meczy z terminem, nie mam czasu przetestować.
            // Módlmy sie ze dziala,
            // Kopiuje ze stackoverflow https://stackoverflow.com/a/12983878/342473

            var sum = 0.0;
            for (long i = 0; i < k; i++)
            {
                sum += Math.Log10(n - i);
                sum -= Math.Log10(i + 1);
            }

            return (long)Math.Pow(10, sum);
        }
    }
}
