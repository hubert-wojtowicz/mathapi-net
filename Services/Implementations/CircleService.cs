﻿using System;
using MathAPI.Services.Interfaces;

namespace MathAPI.Services.Implementation
{
    public class CircleService : ICircleService
    {
        public double CircleField(double r)
        {

            return Math.PI * r * r;
        }

        public double CircleCircuit(double r)
        {

            return 2 * Math.PI * r * r;
        }
    }
}
