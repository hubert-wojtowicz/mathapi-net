﻿using MathAPI.Services.Interfaces;
using System.Collections.Generic;

namespace MathAPI.Services.Implementations
{
    public class FibonacciService : IFibonacciService
    {
        public List<int> GetFibonacci(int len)
        {
            List<int> result = new List<int>();

            int a = 0, b = 1, c;
            for (int i = 2; i < len; i++)
            {
                c = a + b;
                result.Add(c);
                a = b;
                b = c;
            }

            return result;
        }
    }
}
