﻿namespace MathAPI.Services.Interfaces
{
    public interface ICircleService
    {
         double CircleField(double r);
         double CircleCircuit(double r);
    }
}
