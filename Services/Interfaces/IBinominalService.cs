﻿namespace MathAPI.Services.Interfaces
{
    public interface IBinominalService
    {
        long GetBinominal(long n, long k);
    }
}