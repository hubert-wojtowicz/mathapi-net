﻿using System.Collections.Generic;

namespace MathAPI.Services.Interfaces
{
    public interface IFibonacciService
    {
        List<int> GetFibonacci(int len);
    }
}
